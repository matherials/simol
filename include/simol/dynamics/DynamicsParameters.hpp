#ifndef SIMOL_DYNAMICSPARAMETERS_HPP
#define SIMOL_DYNAMICSPARAMETERS_HPP

#include "simol/input/Input.hpp"

namespace simol
{
  class DynamicsParameters
  {
  public:
    DynamicsParameters(Input const& input):
      beta_(input.beta()),
      temperature_(1/beta_),
      deltaTemperature_(input.deltaTemperature()),
      temperatureLeft_(temperature_ + deltaTemperature_),
      temperatureRight_(temperature_ - deltaTemperature_),
      betaLeft_(1/temperatureLeft_),
      betaRight_(1/temperatureRight_),
      gamma_(input.gamma()),
      interactionRatio_(input.interactionRatio()),
      bulkDriving_(input.bulkDriving()),
      mass_(input.mass()),
      nonEqAmplitude_(input.nonEqAmplitude()),
      drift_(input.drift()),
      flux_(input.flux()),
      xi_exchange_(input.xi_exchange()),
      xi_flip_(input.xi_flip()),
      xi_diffusion_(input.xi_diffusion()),
      coupling_(input.coupling()),
      eta_(input.eta()),
      nu_(input.nu())
    {}
    
    const double& temperature() const {return temperature_;}
    const double& temperatureLeft() const {return temperatureLeft_;}
    const double& temperatureRight() const {return temperatureRight_;}
    const double& deltaTemperature() const {return deltaTemperature_;}
    const double& beta() const {return beta_;}
    const double& betaLeft() const {return betaLeft_;}
    const double& betaRight() const {return betaRight_;}
    const double& gamma() const {return gamma_;}
    const double& interactionRatio() const {return interactionRatio_;}
    const double& bulkDriving() const {return bulkDriving_;}
    const double& mass() const {return mass_;}
    const double& nonEqAmplitude() const {return nonEqAmplitude_;}
    const double& drift() const {return drift_;}
    const double& flux() const {return flux_;}
    const double& xi_exchange() const {return xi_exchange_;}
    const double& xi_diffusion() const {return xi_diffusion_;}
    const double& xi_flip() const {return xi_flip_;}
    const double& coupling() const {return coupling_;}
    const double& eta() const {return eta_;}
    const double& nu() const {return nu_;}
  private:
    double beta_;
    double temperature_;
    double deltaTemperature_;
    double temperatureLeft_;
    double temperatureRight_;
    double betaLeft_;
    double betaRight_;
    double gamma_;
    double interactionRatio_;
    double bulkDriving_;
    double mass_;
    double nonEqAmplitude_;
    double drift_;
    double flux_;
    double xi_exchange_;
    double xi_diffusion_;
    double xi_flip_;
    double coupling_;
    double eta_;
    double nu_;
  };

}

#endif
