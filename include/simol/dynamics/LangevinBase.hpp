#ifndef SIMOL_LANGEVINBASE_HPP
#define SIMOL_LANGEVINBASE_HPP

#include "Dynamics.hpp"

namespace simol
{
  class LangevinBase : public Dynamics
  {
    public:
      virtual string dynamicsName() const {return "LangevinBase";}
   
      virtual void simulate (System& syst) const; 
      
      virtual const double& gamma() const;
      virtual const double& xi_exchange() const;
      virtual const double& xi_diffusion() const;
      virtual const double& xi_flip() const;
      virtual bool doMomentaExchange() const;
      virtual bool doMomentaDiffusion() const;
      virtual bool doMomentaFlip() const;
      virtual void updateMomentaExchange (Particle& particle1, Particle& particle2) const;
      virtual void updateMomentaFlip(Particle& particle) const;
      virtual void updateOrsteinUhlenbeck(Particle& particle, double localBeta, double localTimeStep) const;
      // defined only for chains --> see BoundaryLangevin.hpp
      virtual void updateMomentaDiffusion(Particle& /*particle1*/, Particle& /*particle2*/) {assert(false);};
    protected:
      LangevinBase(Input const& input);
      //double gamma_;
      //double xi_;
  };


}


#endif
