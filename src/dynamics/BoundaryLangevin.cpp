#include "simol/dynamics/BoundaryLangevin.hpp"

namespace simol
{
  //#### BoundaryLangevin ####

  ///
  ///Constructor for Langevin dynamics on chains, where there is a thermostat at each end
  BoundaryLangevin::BoundaryLangevin(Input const& input):
    LangevinBase(input)
  {}

  ///
  ///Read-only access for the inverse temperature at the left end
  const double& BoundaryLangevin::betaLeft() const {return parameters_.betaLeft();}
  ///
  ///Read-only access for the inverse temperature at the right end
  const double& BoundaryLangevin::betaRight() const {return parameters_.betaRight();}
  ///
  ///Read-only access for the temperature at the left end
  const double& BoundaryLangevin::temperatureLeft() const {return parameters_.temperatureLeft();}
  ///
  ///Read-only access for the temperature at the right end
  const double& BoundaryLangevin::temperatureRight() const {return parameters_.temperatureRight();}

  ///
  ///Returns the amplitude of the brownian motion at the left end
  double BoundaryLangevin::sigmaLeft() const
  {
    return sqrt(2 * gamma() / parameters_.betaLeft());
  }
  ///
  ///Returns the amplitude of the brownian motion at the right end
  double BoundaryLangevin::sigmaRight() const
  {
    return sqrt(2 * gamma() / parameters_.betaRight());
  }

  
  ///Diffusion process to exchange momenta of two particles
  void BoundaryLangevin::updateMomentaDiffusion(Particle& particle1, Particle& particle2) const
  {
    double random_angle = sqrt(xi_diffusion()*timeStep_) * rng_->scalarGaussian();
    double cos_angle = cos(random_angle);
    double sin_angle = sin(random_angle);
    DVec p1 =  cos_angle*particle1.momentum() + sin_angle*particle2.momentum();
    DVec p2 = -sin_angle*particle1.momentum() + cos_angle*particle2.momentum();
    particle1.momentum() = p1;
    particle2.momentum() = p2; 
  }
  
  void BoundaryLangevin::thermalize(System& syst) const
  {
    for (int i = 0; i < syst.nbOfParticles(); i++)
      verletFirstPart(syst(i));

    syst.computeAllForces();

    for (int i = 0; i < syst.nbOfParticles(); i++)
      verletSecondPart(syst(i));

    for (int i = 0; i < syst.nbOfParticles(); i++)
    {
      double localTemperature = temperatureLeft() + i * deltaTemperature() / syst.nbOfParticles();
      updateOrsteinUhlenbeck(syst(0), 1 / localTemperature, timeStep());
    }
  }

  //------------- standard simulation of chains ----------------
  void BoundaryLangevin::simulate(System& syst) const
  {
    // Verlet part
    for (int iOfParticle = 0; iOfParticle < syst.nbOfParticles(); iOfParticle++)
      verletFirstPart(syst(iOfParticle));

    syst.computeAllForces();

    for (int iOfParticle = 0; iOfParticle < syst.nbOfParticles(); iOfParticle++)
      verletSecondPart(syst(iOfParticle));

    // integrate Ornstein-Uhlenbeck processes at the boundaries
    updateOrsteinUhlenbeck(syst.getParticle(0), betaLeft(), timeStep());
    updateOrsteinUhlenbeck(syst.getParticle(syst.nbOfParticles() - 1), betaRight(), timeStep());

    // perform various possible flip/exchange moves
    // exchange momenta at exponential times
    if (doMomentaExchange())
      for (int iOfParticle = 0; iOfParticle < syst.nbOfParticles() - 1; iOfParticle++)
        updateMomentaExchange(syst(iOfParticle), syst(iOfParticle + 1));
    // flip momenta at exponential times
    if (doMomentaFlip())
      for (int iOfParticle = 0; iOfParticle < syst.nbOfParticles(); iOfParticle++)
	updateMomentaFlip(syst(iOfParticle));
    // continuous exchange process (exclude first and last particles)
    if (doMomentaDiffusion())
      for (int iOfParticle = 1; iOfParticle < syst.nbOfParticles() - 2; iOfParticle++)
	updateMomentaDiffusion(syst(iOfParticle), syst(iOfParticle + 1));
  }
  
void BoundaryLangevin::computeProfileChain(Output& output, System const& syst, long int iOfStep) const
  {    
    output.obsSumFlux().currentValue() = 0;
    
    for (int iOfParticle = 0; iOfParticle < syst.nbOfParticles(); iOfParticle++)
    {
      double potTempBot=1;
      double potTempTop=0;
      double flux = 0;
            
      // computes the different fluxes, plus the configurational temperature
      if (iOfParticle != syst.nbOfParticles()-1)
      {
        if (iOfParticle != 0)
          flux = syst.heatFluxOnAtom(iOfParticle);
        output.obsSumFlux().currentValue() += flux;
        
	potTempTop = pow(syst(iOfParticle).energyGrad(0), 2);
        potTempBot = syst(iOfParticle).energyLapla();
      }
      
      // updates the statstics on the the profile of some quantities along the chain
      output.appendPotTempTopProfile(potTempTop, iOfStep, iOfParticle);
      output.appendPotTempBotProfile(potTempBot, iOfStep, iOfParticle);
      output.appendFluxProfile(flux, iOfStep, iOfParticle);
      output.appendKinTempProfile(2 * syst(iOfParticle).kineticEnergy(), iOfStep, iOfParticle); 
      output.appendExtFluxProfile(flux * syst.lagrangeMultiplier(), iOfStep, iOfParticle);
    }
    output.obsSumFlux().currentValue() /= (syst.nbOfParticles() - 2.);
    
  }
    
  //------------------------ Constrained Boundary Langevin ----------------------
  
  
  ///Constructor for the Constrained Langevin Dynamics with thermostats everywhere
  ConstrainedBoundaryLangevin::ConstrainedBoundaryLangevin(Input const& input):
    BoundaryLangevin(input)
  {}
  
  void ConstrainedBoundaryLangevin::simulate(System& syst) const
  {
    syst.lagrangeMultiplier() = 0;
    
    // flucutations at the boundaries 
    updateOrsteinUhlenbeck(syst.getParticle(0), betaLeft(), timeStep()/2);
    updateOrsteinUhlenbeck(syst.getParticle(syst.nbOfParticles() - 1), betaRight(), timeStep()/2);
        
    // deterministic part, including Lagrange multipliers 
    for (int iOfParticle = 0; iOfParticle < syst.nbOfParticles(); iOfParticle++)
      updateMomentum(syst(iOfParticle));
    syst.enforceConstraint(flux(), parameters(), true);
    
    for (int iOfParticle = 0; iOfParticle < syst.nbOfParticles(); iOfParticle++)
      updatePosition(syst(iOfParticle));
    syst.computeAllForces();

    for (int iOfParticle = 0; iOfParticle < syst.nbOfParticles(); iOfParticle++)
      updateMomentum(syst(iOfParticle));
    syst.enforceConstraint(flux(), parameters(), true);

    // Hamiltonian part of the Lagrange multiplier, approximated by (J^{n+1}-J^n)/\Delta t where J^{n+1} = current after Verlet step
    syst.lagrangeMultiplier() /= timeStep();
    //cout << " LM from Hamiltonian part : " << syst.lagrangeMultiplier() << endl;

    // fluctations at the boundaries; does not affect the bulk current 
    updateOrsteinUhlenbeck(syst.getParticle(0), betaLeft(), timeStep()/2);
    updateOrsteinUhlenbeck(syst.getParticle(syst.nbOfParticles() - 1), betaRight(), timeStep()/2);
        
    // flips and exchanges; affects the bulk current
    if (doMomentaExchange())
      {
	for (int iOfParticle = 0; iOfParticle < syst.nbOfParticles() - 1; iOfParticle++)
	  updateMomentaExchange(syst(iOfParticle), syst(iOfParticle + 1));
      }
    
    if (doMomentaFlip())
      {
	for (int iOfParticle = 0; iOfParticle < syst.nbOfParticles(); iOfParticle++)
	  updateMomentaFlip(syst(iOfParticle));
      }

    // exchange noise which preserves energy but not momentum; excludes first and last particles
    if (doMomentaDiffusion())
      {
	for (int iOfParticle = 1; iOfParticle < syst.nbOfParticles() - 2; iOfParticle++)
	  updateMomentaDiffusion(syst(iOfParticle), syst(iOfParticle + 1));
	syst.UpdateLagrangeMultiplierDiffusion(flux(), parameters());
      }
  }

}
