#include "simol/dynamics/LangevinBase.hpp"

namespace simol
{
  ///Constructs a purely virtual Dynamics for Dynamics using RNGs
  LangevinBase::LangevinBase(Input const& input):
    Dynamics(input)
  {}

  ///
  ///Read-only accessor of the intensity of the O-U process
  const double& LangevinBase::gamma() const {return parameters_.gamma();}

  ///Read-only accessor for xi
  const double& LangevinBase::xi_exchange() const {return parameters_.xi_exchange();}
  const double& LangevinBase::xi_flip() const {return parameters_.xi_flip();}
  const double& LangevinBase::xi_diffusion() const {return parameters_.xi_diffusion();}
  ///
  ///Returns true if the dynamics involves a Poisson process (momenta exchange)
  bool LangevinBase::doMomentaExchange() const
  {return xi_exchange() > 0;}
  ///
  ///Returns true if the dynamics involves a local diffusion process on momenta
  bool LangevinBase::doMomentaDiffusion() const
  {return xi_diffusion() > 0;}
  ///
  ///Returns true if the dynamics involves a Poisson process (momenta flip)
  bool LangevinBase::doMomentaFlip() const
  {return xi_flip() > 0;}
  ///
  ///Exchanges the momenta of the 2 particles if the time has come
  void LangevinBase::updateMomentaExchange(Particle& particle1, Particle& particle2) const
  {
    if (particle2.countdown() < 0)
    {
      DVec temp = particle2.momentum();
      particle2.momentum() = particle1.momentum();
      particle1.momentum() = temp;
      particle2.countdown() = rng_->scalarExponential(); 
    }
    else
      particle2.countdown() -= xi_exchange() * timeStep_;
  }
  ///
  ///Flip of momenta if the time has come
  void LangevinBase::updateMomentaFlip(Particle& particle) const
  {
    if (particle.countdown() < 0)
    {
      particle.momentum() *= -1.;
      particle.countdown() = rng_->scalarExponential(); 
    }
    else
      particle.countdown() -= xi_flip() * timeStep_;
  }
  
  ///
  ///Analytical integration of an Orstein-Uhlenbeck process of inverse T "localBeta"
  void LangevinBase::updateOrsteinUhlenbeck(Particle& particle, double localBeta, double localTimeStep) const
  {
    double alpha = exp(- parameters_.gamma() / particle.mass() * localTimeStep);
    particle.momentum() = alpha * particle.momentum() + sqrt((1 - pow(alpha, 2)) / localBeta * particle.mass()) * rng_->gaussian();
  }
  
  void LangevinBase::simulate(System& syst) const
  {
    for (int iOfParticle = 0; iOfParticle < syst.nbOfParticles(); iOfParticle++)
      verletFirstPart(syst(iOfParticle));
    syst.computeAllForces();
    for (int iOfParticle = 0; iOfParticle < syst.nbOfParticles(); iOfParticle++)
      verletSecondPart(syst(iOfParticle));
  }
  



}
